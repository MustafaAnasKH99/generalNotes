Dear Costumer,

Thank you for requesting a loan from Fransa Bank. We are writing today to inform you that unfortunately the loan you requested has not been accepted by Fransa Bank. We would recommend you to please review the requirements and make sure your request meets them. We are happy to recieve this loan request once you do so and ready to submit again.

Please contact us in case you have anymore questions.
