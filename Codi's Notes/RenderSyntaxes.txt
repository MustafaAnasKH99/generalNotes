ReactDOM.render(ComponentToRender, targetedNode);
As you would expect, ReactDOM.render() must be called 
after the JSX element declarations, just like how you
 must declare variables before using them.
 
 forms of components:
 const MyCompnent = function(){
 	return (<div></div>)
 }
 
 class MyCompnent extends React.Component{
 	constructor(props){
 	super(props)
 	}
 	
 	render(){
 		return(
 			<div></div>
 		)
 	}
 };
 
 
 
 Note: As of React v15.5.0, PropTypes is imported independently 
 from React, like this:
 
 import React, { PropTypes } from 'react';
 
 
 A stateless functional component is any function you write which
 accepts props and returns JSX.
 
 A stateless component, on the other hand, is a class that extends
 React.Component, but does not use internal state.
 
 
 MyComponent.propTypes = { handleClick: PropTypes.func.isRequired }
 
 --------------------------------------------------------------------
 
 Create a stateful Component:
 
 
 class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'freeCodeCamp'
    }
  }
  render() {
    return (
      <div>
        { /* change code below this line */ }
        <h1>{this.state.name}</h1>
        { /* change code above this line */ }
      </div>
    );
  }
};












 
 
 
 
 
 
 
 
 
